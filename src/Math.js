export default class Math {
  static add(term1, term2) {
    return term1 + term2;
  }

  static subtract(minuend, subtrahend) {
    return minuend - subtrahend;
  }

  static multiply(multiplier, multiplicand) {
    return multiplier * multiplicand;
  }

  static divide(dividend, divisor) {
    return dividend / divisor;
  }
}
