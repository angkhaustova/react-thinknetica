import React, { Component } from 'react';
import Arithmetic from '~/src/Math';

class Math extends Component {
  render() {
    return (
      <div>
        <p>2 + 2 is {Arithmetic.add(2, 2)}</p>
        <p>2 - 2 is {Arithmetic.subtract(2, 2)}</p>
        <p>2 * 2 is {Arithmetic.multiply(2, 2)}</p>
        <p>2 / 2 is {Arithmetic.divide(2, 2)}</p>
      </div>
    );
  }
}

export default Math;
