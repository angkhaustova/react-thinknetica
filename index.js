import React from 'react';
import ReactDOM from 'react-dom';
import Math from '~/src/components/Math';

ReactDOM.render(<Math />, document.getElementById('root'));
